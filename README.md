# NYT Top News Visualization
The [NYT API](https://developer.nytimes.com) provides us with a wealth of data via its 10 APIs, some of the data
dating all the way back to 1851. The code in this repo generated a visualization that shows the top stories for
the day, separated by section. Hovering over an article will show the associated
tags and terms for that article, as well as geographical data.

## Setup
``` bash
npm install

# serve with hot reload at localhost:8080
npm run dev

